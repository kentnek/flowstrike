import "./source";
import "./source/explain"
import "./source/phantom"

import phantom from 'phantom';

function initPhantom() {
    return async(context) => {
        let phantomInstance = await phantom.create();
        context.page = await phantomInstance.createPage();
        //await context.page.setting("userAgent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0");
    };
}

$explain(
    { a: 1, b: 2 },
    $.log($.hash({ a: __.b })),
    $.log($.hash(__))
);
