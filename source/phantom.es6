if (!global.$) throw new Error("Must import Flowstrike first!");

import * as Generators from './lib/phantom/generators';
$.importGenerators(Generators);
