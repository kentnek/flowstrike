'use strict';

import ConsoleLogger from "./logger/ConsoleLogger";
import Expression from "./interpreter/base/Expression";

async function explain(...steps) {
    try {
        let exp = new Expression(...steps);
        exp.setLogger(ConsoleLogger);
        return await exp.evaluate();
    } catch (error) {
        console.error(error);
    }
}

global.$explain = explain;
