'use strict';

import 'colour';
import BaseExpression from "../interpreter/base/BaseExpression";
import Resolvable from "../interpreter/resolvable/Resolvable";

let originalLog = console.log;

let changeLogLevel = (level, offset) => {
    if (level === 0) {
        originalLog();
        console.log = originalLog;
    } else {
        console.log = (...args) => {
            let prefix = '│   '.repeat(level);
            if (offset !== 0) prefix = prefix.slice(0, offset);
            prefix = prefix.slice(0, -1);
            args.unshift(prefix.gray);
            originalLog(...args)
        };
    }
};

let currentOneLiner = null;

export default function Log(exp, level) {
    if (exp.identifier === "log") return;

    if (typeof(exp) === "string") {
        changeLogLevel(level);
        console.log(exp.gray);
        changeLogLevel(level + 1);
        return;
    }

    let { type = "", displayName, extra = "", oneLiner } = generateLog(exp);
    if (type === "Step" && !displayName) return;

    let log = [type.yellow, displayName, extra.white].filter(x => x).join(" ");

    if (oneLiner) {
        currentOneLiner = oneLiner;
    } else {
        if (!currentOneLiner) {
            changeLogLevel(level);
            console.log(log);
            changeLogLevel(level + 1);
        } else {
            changeLogLevel(level - 1, currentOneLiner.offset);
            console.log(currentOneLiner.prefix, log, currentOneLiner.suffix);
        }

        currentOneLiner = null;
    }
};

function generateLog(exp) {
    let ret = {};

    if (exp instanceof BaseExpression) {
        ret.type = exp.constructor.name;
        ret.displayName = exp.identifier ? exp.identifier.bold : null;
    } else if (exp instanceof Function || exp instanceof Resolvable) {
        ret.type = "Step";
        ret.displayName = exp.identifier || exp.name;
    } else {
        ret.type = "Value";
        ret.extra = JSON.stringify(exp);
    }

    let map = exp.argumentMap;

    if (ret.type === "Assign") {
        if (exp.flow.steps.length === 1 && !(exp.flow.steps[0] instanceof BaseExpression)) {
            ret.oneLiner = {
                prefix: "{".blue,
                suffix: `} => ${exp.key}`.blue,
                offset: -2
            }
        } else {
            ret.extra = `=> ${exp.key}`.blue;
        }

    } else if (ret.type === "Throw") {
        ret.extra = exp.resolvedError;

    } else if (ret.type === "MultiPage") {
        ret.extra = exp.pageDescription;

    } else if (ret.type === "If" && exp.decision !== undefined) {
        ret.extra = ": ".gray + String(exp.decision).white;

    } else if (ret.type === "PartitionBy") {
        ret.extra = exp.partitionKey || exp.groupGenerator.fn.name;

    } else if (exp.constructor.name === "ResolvableObject") {
        ret.displayName = "convert to object";
        //ret.extra = JSON.stringify(exp.resolvedObject);

    } else if (ret.type === "Define") {
        ret.extra = `#${exp.variableName} = `.gray + exp.resolvedValue.white;

    } else if (ret.type === "Race") {
        ret.extra = `timeout = `.gray + exp.timeout.white;

    } else if (ret.displayName === "defineContext") {
        ret.extra = `#${map.name} = `.gray + map.value.white;

    } else if (ret.displayName === "join") {
        ret.extra = `= `.gray + map["...s"].join("").white;

    } else if (ret.displayName === "joinDash") {
        ret.extra = `= `.gray + map["...s"].join("-").white;

    } else if (ret.displayName === "mul") {
        ret.extra = `${map.a} * ${map.b} = ${map.a * map.b}`.white;

    } else if (ret.displayName === "iif") {
        ret.extra = `${map.predicate} => ${map.predicate ? map.trueValue : map.falseValue}`.white;

    } else if (ret.displayName === "evaluate") {
        ret.extra = "{ ".gray + map.fn.white + " }".gray;

    } else if (ret.displayName === "js") {
        ret.extra = "{ ".gray + map.script.white + " }".gray;

    } else if (map) {
        ret.extra = argumentMapToString(map).white;
    }

    return ret;
}

function argumentMapToString(map) {
    let str = [];

    for (let [name, value] of Object.entries(map)) {
        if (Array.isArray(value)) value = "[" + value.join(", ") + "]";
        str.push(`${name}: `.gray + value.white);
    }

    return "(".gray + str.join(", ".gray) + ")".gray; // "(arg: value, arg: value...)"
}
