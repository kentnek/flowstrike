'use strict';


import "./interpreter/extensions/Assign";
import 'core-js/fn/object/entries';

//region Expressions

import requireDir from 'require-dir';
let Expressions = requireDir("./interpreter/expressions/");

for (let [name, expression] of Object.entries(Expressions)) {
    global[name] = new Proxy(expression.default, {
        apply (target, thisArg, argumentsList) {
            return new target(...argumentsList);
        }
    });
}

//endregion


//region Placeholder

import Placeholder from './interpreter/resolvable/Placeholder';
global.__ = Placeholder.create();
global.__context = Placeholder.create("context");

//endregion


//region Step Generators & Steps

import Expression from "./interpreter/base/Expression";
import BaseExpression from "./interpreter/base/BaseExpression";
import ResolvableObject from "./interpreter/resolvable/ResolvableObject";
import ResolvableEval from "./interpreter/resolvable/ResolvableEval";

async function evaluateExpression(...steps) {
    try {
        return await new Expression(...steps).evaluate();
    } catch (error) {
        console.error(error);
    }
}

const $ = function resolve(...args) {

    if (args.length === 1 && !(args[0] instanceof BaseExpression)) {
        let object = args[0];
        if (typeof(object) === "function") return ResolvableFn.defer(object);
        if (typeof(object) === "string") return new ResolvableEval(object);
        return ResolvableObject.wrap(object);
    } else {
        return evaluateExpression(...args);
    }

};

import ResolvableFn from "./interpreter/resolvable/ResolvableFn";

$.importGenerator = function (name, generator) {
    name = name.replace("$", "");
    if ($[name]) throw new Error("Function already exists in $.");
    $[name] = ResolvableFn.defer(generator);
};

$.importGenerators = function (generators) {
    for (let [name, fn] of Object.entries(generators)) {
        $.importGenerator(name, fn);
    }
};

import * as CoreGenerators from './lib/core/generators';
$.importGenerators(CoreGenerators);

import TimeoutError from "./lib/core/TimeoutError";
$.TimeoutError = TimeoutError;

global.$ = $;

//endregion


