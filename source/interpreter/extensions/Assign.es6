import Expression from "../base/Expression";
import _Assign from "../expressions/Assign";

import ResolvableFn from "../resolvable/ResolvableFn";
import ResolvableEval from "../resolvable/ResolvableEval";
import ResolvableObject from "../resolvable/ResolvableObject";

function AssignTo(key) {
    return new _Assign(key, this);
}

Expression.prototype.Assign = AssignTo;
ResolvableFn.prototype.Assign = AssignTo;
ResolvableEval.prototype.Assign = AssignTo;
ResolvableObject.prototype.Assign = AssignTo;


