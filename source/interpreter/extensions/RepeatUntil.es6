import Expression from "../base/Expression";
import Continue from "../expressions/Continue";

Expression.prototype.RepeatUntil = function (predicateFn) {
    let expList = this.flow.expressions;

    if (expList[expList.length - 1] instanceof Continue) {
        throw new Error("Continue already exists at the end of this expression.");
    }

    let until = Continue(predicateFn).negate();
    until.extra = "until ".white + predicateFn.name.bold;

    expList.push(until);

    return this;
};