"use strict";

import Resolvable from "../resolvable/Resolvable";

export default class Counter extends Resolvable(Object) {
    constructor() {
        super();
        this.currentValue = 0;
    }

    resolve() {
        return ++this.currentValue;
    }
}