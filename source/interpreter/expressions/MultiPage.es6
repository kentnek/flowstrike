'use strict';

import Expression from "../base/Expression";
import Incomplete from "../base/Incomplete";

import Task from "./Task";
import Append from "./Append";
import If from "./If";
import Pass from "./Pass";
import Continue from "./Continue";

//const PAGE = Symbol("page");

class MultiPage extends Expression {
    constructor(pageDescription, steps) {
        super();
        this.pageDescription = pageDescription;
        this.builder = steps;

        this._super = {
            evaluate: super.evaluate.bind(this)
        };

    }

    async evaluate(context, input) {
        let results = [];
        context.currentPage = 1;
        this.level += 1;

        let evaluateNow = () => this._super.evaluate(context, input);

        while (true) {
            if (this.builder["BeforePage"]) {
                this.log(`Before page:`, this.level - 1);
                this.flow.setSteps(this.builder["BeforePage"]);
                await evaluateNow();
            }

            this.log(`On page ${context.currentPage}`, this.level - 1);

            this.flow.setSteps(this.builder["OnPage"]);
            results = results.concat(await evaluateNow());

            this.log(`Checking:`, this.level - 1);
            this.flow.setSteps(this.builder["Check"]);

            if (await evaluateNow() === true) {
                if (this.builder["Next"]) {
                    this.log(`Next:`, this.level - 1);
                    this.flow.setSteps(this.builder["Next"]);
                    await evaluateNow();
                }
            } else {
                break;
            }

            context.currentPage++;
        }

        Reflect.deleteProperty(context, "currentPage");

        return results;
    }


}

export default class MultiPageBuilder extends Incomplete {
    constructor(pageDescription) {
        super();

        this.pageDescription = pageDescription;
        this.steps = [];
    }

    BeforePage(...steps) {
        this.steps["BeforePage"] = steps;
        return this;
    }

    OnPage(...steps) {
        this.steps["OnPage"] = steps;
        return this;
    }

    Check(...steps) {
        this.steps["Check"] = steps;
        return this;
    }

    Next(...steps) {
        this.steps["Next"] = steps;
        return this;
    }

    build() {
        return new MultiPage(this.pageDescription, this.steps);
    }
}