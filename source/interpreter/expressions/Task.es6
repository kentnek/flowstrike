'use strict';

import Expression from "../base/Expression";


/**
 * A Task is an Expression with an identifier.
 */

export default class Task extends Expression {
    constructor(identifier, ...steps) {

        if (typeof(identifier) !== "string") {
            throw new TypeError("First parameter of Task must be a string.")
        }

        super(...steps);

        this.identifier = identifier;

        this.preEvaluate = null;
        this.postEvaluate = null;
        this.onError = null;

        this.breakable = true;

        this._super = {
            evaluate: super.evaluate.bind(this)
        }
    }

    async evaluate(context, input) {

        try {
            if (this.preEvaluate) this.preEvaluate(this.identifier);
            let output = await this._super.evaluate(context, input);
            if (this.postEvaluate) this.postEvaluate(this.identifier);

            return output;
        } catch (error) {
            if (this.onError) this.onError(this.identifier, error);
            throw error;
        }
    }
}