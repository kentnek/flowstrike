'use strict';

import Expression from "../base/Expression";
import Incomplete from "../base/Incomplete";

class MergeByKey extends Expression {
    postTransform(sourceArray, targetArray) {
        if (!Array.isArray(sourceArray) || !Array.isArray(targetArray)) {
            throw new TypeError("Merge block only accepts arrays as input and block output.");
        }

        let targetMap = new Map();
        for (let target of targetArray) {
            let groupId = target[this.targetKey];
            if (groupId === undefined) throw new Error(`Target element missing key ${this.targetKey}`);

            targetMap.set(groupId, target);
            Reflect.deleteProperty(target, this.targetKey);
        }

        for (let source of sourceArray) {
            let groupId = source[this.sourceKey];
            if (groupId === undefined) throw new Error(`Source element missing key ${this.sourceKey}`);

            Object.assign(source, targetMap.get(groupId));
        }

        return sourceArray;
    }
}

export default class MergeBuilder extends Incomplete {
    constructor(sourceKey, targetKey) {
        super();

        this.sourceKey = sourceKey;
        this.targetKey = targetKey;
    }

    With(...steps) {
        let merge = new MergeByKey(...steps);
        merge.sourceKey = this.sourceKey;
        merge.targetKey = this.targetKey;
        return merge;
    }
}