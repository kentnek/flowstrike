'use strict';

import Expression from "../base/Expression";

const TAG = Symbol("map_element_tag");
const MAX_ELEMENT_SHOWN = Symbol("max_element_shown");

export default class MapArray extends Expression {
    constructor(...steps) {
        super(...steps);

        this[TAG] = "element";

        this._super = {
            evaluate: super.evaluate.bind(this)
        };

        this.breakable = true;
        this[MAX_ELEMENT_SHOWN] = 3;
    }

    preTransform(input, index, length) {
        return input;
    }

    async evaluate(context, inputArray) {
        if (!Array.isArray(inputArray)) {
            throw new Error(`"Map" expects an array as input.`);
        }

        let results = [];

        let index = 0, length = inputArray.length;
        let max = this[MAX_ELEMENT_SHOWN];

        for (let input of inputArray) {
            this.log(`${this[TAG]} #${index + 1}`, this.level - 1);

            results.push(await this._super.evaluate(context, this.preTransform(input, index, length)));
            index++;

            if (index >= max) {
                this.setLogEnabled(false);
            } else if (this.flow.steps.length > 1) {
                this.log();
            }
        }

        this.setLogEnabled(true);

        if (index > max) {
            this.log();
            this.log(`and ${index - max} more`, this.level - 1);
            this.log("", this.level - 1);
        }

        return results;
    }

    tag(tag) {
        this[TAG] = tag;
        return this;
    }

    expand(max) {
        this[MAX_ELEMENT_SHOWN] = max ? max : 1000;
        return this;
    }


}