'use strict';

import BaseExpression from "../base/BaseExpression";
import Resolvable from "../resolvable/Resolvable";
import ResolvableObject from "../resolvable/ResolvableObject";

export default class PartitionBy extends BaseExpression {

    constructor(key) {
        super();

        if (typeof(key) === "string") {
            this.partitionKey = key;
        } else if (key instanceof Resolvable) {
            this.groupGenerator = key;
        }

    }

    Wrap(key, extractKey = true) {
        this.wrapKey = key;
        this.extractKey = extractKey;
        return this;
    }

    evaluate(context, inputArray) {
        if (!Array.isArray(inputArray)) {
            throw new TypeError("PartitionBy block only accepts an array as input.");
        }

        let partitionMap = new Map();

        for (let input of inputArray) {
            let partitionId;

            if (this.partitionKey) { // partition by key
                partitionId = input[this.partitionKey];
                if (this.extractKey) Reflect.deleteProperty(input, this.partitionKey);
            } else if (this.groupGenerator) {
                partitionId = ResolvableObject.resolve(this.groupGenerator, context, input);
            }

            if (!partitionMap.has(partitionId)) partitionMap.set(partitionId, []);
            partitionMap.get(partitionId).push(input);
        }

        let output = [];

        for (let [id, partition] of partitionMap.entries()) {
            if (this.wrapKey) {
                partition = {[this.wrapKey]: partition};
                if (this.extractKey) partition[this.partitionKey || "$group"] = id; // extract partition id out
            }
            output.push(partition);
        }

        return output;
    }
}