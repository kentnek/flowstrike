import Expression from "../base/Expression";

export default class Append extends Expression {
    postTransform(scopeInput, evaluateResult) {
        if (Array.isArray(scopeInput)) {
            if (evaluateResult !== null) scopeInput = scopeInput.concat(evaluateResult);
            return scopeInput;
        } else {
            throw new TypeError("Append block only accepts an array as input.");
        }
    }
}