'use strict';

import MapArray from "./MapArray";

export default class MapWithIndex extends MapArray {
    preTransform(input, index, length) {
        return {
            value: input,
            index: index,
            length: length
        };
    }
}