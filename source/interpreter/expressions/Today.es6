'use strict';

const DATE = Symbol("date");
const FORMAT = Symbol("format");

import Moment from 'moment';

export default class Today {
    constructor(format = "MM/DD/YYYY", moment) {
        this[FORMAT] = format;
        this[DATE] = moment || Moment(new Date());
    }

    valueOf() {
        return this[DATE].format(this[FORMAT]);
    }

    subtract(value, type = "day") {
        return new Today(this[FORMAT], this[DATE].subtract(value, type));
    }
}