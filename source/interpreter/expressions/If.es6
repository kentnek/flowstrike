'use strict';

import Expression from "../base/Expression";
import Predicate from "../base/Predicate";
import LinearFlow from "../flow/LinearFlow";

export default class If extends Predicate(Expression) {

    constructor(predicateFn) {
        super(predicateFn);

        this._super = {
            evaluate: super.evaluate.bind(this)
        }
    }

    generateFlow() {
        return new ConditionalFlow();
    }

    Then(...steps) {
        this.flow.setThenSteps(steps);
        return this;
    }

    Else(...steps) {
        this.flow.setElseSteps(steps);
        return this;
    }

    async evaluate(context, input) {
        this.flow.decide(await this.test(context, input));
        return await this._super.evaluate(context, input);
    }
}

class ConditionalFlow extends LinearFlow {
    constructor() {
        super();
    }

    setThenSteps(steps) {
        this.thenSteps = steps;
    }

    setElseSteps(steps) {
        this.elseSteps = steps;
    }

    decide(input) {
        this.steps = input ? this.thenSteps : this.elseSteps;
    }
}