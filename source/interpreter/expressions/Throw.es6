'use strict';

import BaseExpression from "../base/BaseExpression";
import Predicate from "../base/Predicate";
import Resolvable from "../resolvable/Resolvable";
import ResolvableObject from "../resolvable/ResolvableObject";

export default class Throw extends Resolvable(Predicate(BaseExpression)) {

    constructor(errorToThrow, predicateFn) {
        super(predicateFn);
        this.errorToThrow = errorToThrow;
    }

    resolve(context, input) {
        this.resolvedError = ResolvableObject.resolve(this.errorToThrow, context, input);
        return this;
    }

    async evaluate(context, input) {
        if (await this.test(context, input)) throw this.resolvedError;
    }

}