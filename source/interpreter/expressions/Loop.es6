'use strict';

import Expression from "../base/Expression";
import Incomplete from "../base/Incomplete";
import LinearFlow from "../flow/LinearFlow";

class Loop extends Expression {
    constructor(times = Infinity, steps) {
        super(...steps);
        this.flow.times = times;
        this.breakable = true;
    }

    generateFlow() {
        return new LoopFlow(this.steps);
    }
}

export default class LoopBuilder extends Incomplete {
    constructor(times) {
        super();
        this.times = times;
    }

    Do(...steps) {
        return new Loop(this.times, steps);
    }
}

class LoopFlow extends LinearFlow {
    constructor(steps) {
        super(steps);
        this.loopCount = 0;
        this.times = Infinity;
    }

    hasNext() {
        return this.steps && ((this.position + 1 < this.steps.length) || (this.loopCount < this.times));
    }

    next() {
        if (super.hasNext()) {
            return super.next();
        } else {
            if (++this.loopCount < this.times && !this.hasTerminated) {
                this.moveToStart();
                return super.next();
            } else {
                return LinearFlow.NO_OP;
            }

        }
    }

    reset() {
        this.loopCount = 0;
        super.reset();
    }

    terminate() {
        this.hasTerminated = true;
        super.terminate();
    }
}