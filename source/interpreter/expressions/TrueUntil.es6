"use strict";

import Counter from "./Counter";

export default class TrueUntil extends Counter {
    constructor(untilValue) {
        super();
        this.untilValue = untilValue;
    }

    resolve() {
        let value = super.resolve();
        return value < this.untilValue;
    }
}