'use strict';

import Expression from "../base/Expression";
import {SwitchFlow} from "./Switch";

import {waitFor} from "../../lib/utils/async";
import TimeoutError from "../../lib/core/TimeoutError";

export default class Race extends Expression {
    constructor(timeout) {
        super();
        this.timeout = timeout;
        this.conditions = [];

        this._super = {
            evaluate: super.evaluate.bind(this)
        }
    }

    generateFlow() {
        return new SwitchFlow();
    }

    On(condition, ...steps) {
        this.conditions.push(condition);
        this.flow.setCase(this.conditions.length - 1, steps);
        return this;
    }

    Timeout(...steps) {
        this.flow.setCase("timeout", steps);
        return this;
    }

    async evaluate(context, input) {
        let conditionIndex;

        try {
            await waitFor(async () => {
                for (let [index, condition] of this.conditions.entries()) {
                    if (await this.evaluateStep(condition, context, input)) {
                        conditionIndex = index;
                        return true;
                    }
                }

                return false;

            }, this.timeout);

        } catch (e) {
            if (e instanceof TimeoutError) {
                conditionIndex = "timeout";
            } else {
                throw e;
            }
        }

        if (conditionIndex === "timeout") {
            this.log(`On timeout`);
        } else {
            this.log(`On ${this.conditions[conditionIndex].toString()}`);
        }

        this.flow.decide(conditionIndex);
        return await this._super.evaluate(context, input);
    }
}