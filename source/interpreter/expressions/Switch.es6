'use strict';

import Expression from "../base/Expression";
import LinearFlow from "../flow/LinearFlow";

export default class Switch extends Expression {
    constructor(switchStep) {
        super();
        this.switchStep = switchStep;

        this._super = {
            evaluate: super.evaluate.bind(this)
        }
    }

    generateFlow() {
        return new SwitchFlow();
    }

    Case(value, ...steps) {
        this.flow.setCase(value, steps);
        return this;
    }

    Default(...steps) {
        this.flow.setDefault(steps);
        return this;
    }

    async evaluate(context, input) {
        let switchInput = await this.evaluateStep(this.switchStep, context, input);
        this.log(`Case ${JSON.stringify(switchInput)}`);

        this.flow.decide(switchInput);
        return await this._super.evaluate(context, input);
    }
}

const DEFAULT_CASE = Symbol("default");

export class SwitchFlow extends LinearFlow {
    constructor() {
        super();
        this.cases = {};
    }

    setCase(value, steps) {
        this.cases[value] = steps;
    }

    setDefault(steps) {
        this.cases[DEFAULT_CASE] = steps;
    }

    decide(input) {
        this.steps = this.cases[input] || this.cases[DEFAULT_CASE];
    }
}