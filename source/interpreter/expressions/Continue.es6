'use strict';

import FlowControl from "../base/FlowControl";

export default class Continue extends FlowControl {
    alter(parent) {
        let levelOffset = 1;

        while (parent) {
            if (parent.breakable) {
                parent.flow.moveToStart();
                break;
            }
            parent = parent.parent;
            levelOffset++;
        }

        this.log("", this.level - levelOffset);
    }
}