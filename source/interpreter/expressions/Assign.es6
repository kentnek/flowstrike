import Expression from "../base/Expression";

export default class Assign extends Expression {
    constructor(key, ...steps) {
        super(...steps);
        this.key = key;
        this.breakable = true;
    }

    postTransform(scopeInput, evaluateResult) {
        if (typeof scopeInput !== "object") throw new TypeError("Assign block only accepts an object as input.");
        scopeInput[this.key] = evaluateResult;
        return scopeInput;
    }
}