'use strict';

import FlowControl from "../base/FlowControl";

export default class Break extends FlowControl {
    alter(parent) {
        while (parent) {
            parent.flow.terminate();
            if (parent.breakable) return;
            parent = parent.parent;
        }

        throw new Error(`'${this.constructor.name}' must be inside a breakable expression.`);
    }
}