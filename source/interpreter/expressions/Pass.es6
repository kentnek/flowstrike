import Expression from "../base/Expression";

export default class Pass extends Expression {
    postTransform(scopeInput, evaluateResult) {
        return scopeInput;
    }
}