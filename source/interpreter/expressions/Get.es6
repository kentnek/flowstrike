"use strict";

import Resolvable from "../resolvable/Resolvable";

export default class Get extends Resolvable(Object) {
    constructor(name) {
        super();
        this.variableName = name;
    }

    resolve(context) {
        return context.getScope().get(this.variableName);
    }
}