"use strict";

import BaseExpression from "../base/BaseExpression";
import Resolvable from "../resolvable/Resolvable";

export default class Define extends Resolvable(BaseExpression) {
    constructor(name, value) {
        super();
        this.variableName = name;
        this.value = value;
    }

    resolve(context, input) {
        this.resolvedValue = this.value;

        if (this.resolvedValue instanceof Resolvable) {
            this.resolvedValue = this.value.resolve(context, input);
        }

        return this;
    }

    evaluate(context) {
        context.getScope().set(this.variableName, this.resolvedValue);
    }
}