'use strict';

import FlowControl from "../base/FlowControl";

export default class Exit extends FlowControl {
    alter(parent) {
        while (parent) {
            parent.flow.terminate();
            parent = parent.parent;
        }
    }
}