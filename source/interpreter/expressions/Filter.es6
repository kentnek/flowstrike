'use strict';

import BaseExpression from "../base/BaseExpression";
import Predicate from "../base/Predicate";

export default class Filter extends Predicate(BaseExpression) {

    async evaluate(context, inputArray) {
        if (!Array.isArray(inputArray)) throw new Error(`"Filter" expects an array as input.`);

        let results = [];

        for (let input of inputArray) {
            if (await this.test(context, input)) results.push(input);
        }

        return results;
    }

}