export function generateArgumentMap(fn, args, mask) {
    let argNames = getArgumentNames(fn);

    let map = {};
    let nameIndex = 0;

    for (let argument of args) {
        let name = argNames[nameIndex];
        let value = toString(argument);

        if (mask === true && !name.startsWith('$')) value = "[masked]";
        if (name.startsWith('$')) name = name.slice(1);

        if (name && name.startsWith("...")) {
            if (!map[name]) map[name] = [];
            map[name].push(value);
        } else {
            map[name] = value;
            nameIndex++;
        }
    }

    return map;
}

function getArgumentNames(func) {
    let funcString = func.toString();
    funcString = funcString.slice(0, funcString.indexOf("{"));

    let args;

    if (!funcString.includes("(")) {
        args = funcString;
    } else {
        args = func.toString().match(/\((.*)\)/)[1];
    }

    return args
        .split(',')
        .map(a => a
            .replace(/\/\*.*\*\//, '') // Removes comments
            .replace(/=.*/, '') // Removes default values
            .trim()
        )
        .filter(a => a); // filter out null/undefined
}

function toString(x) {
    if (typeof(x) === "function") {
        if (x.name) return x.name;

        let fnContent = x.toString();
        fnContent = fnContent
            .substring(fnContent.indexOf("{") + 1, fnContent.lastIndexOf("}"))
            .replace("\n", "")
            .trim();

        return fnContent;
    }

    if (typeof(x) === "string") return shorten(x);
    return String(x);
}

function shorten(s) {
    return s.length < 50 ? s : s.substring(0, 50) + "…";
}
