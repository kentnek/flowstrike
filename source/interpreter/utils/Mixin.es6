"use strict";

const ORIGINAL_MIXIN = Symbol("ORIGINAL_MIXIN");
const MIXIN_REF = Symbol("MIXIN_REF");
const CACHED_APPLICATION_REF = Symbol("CACHED_APPLICATION_REF");

export default (mixin, name) => Cached(HasInstance(BaseMixin(mixin, name)));

const wrap = (mixin, wrapper, name) => {
    Object.setPrototypeOf(wrapper, mixin);
    if (!mixin[ORIGINAL_MIXIN]) mixin[ORIGINAL_MIXIN] = mixin;

    if (name) {
        wrapper.displayName = name;
    } else if (!wrapper.displayName) {
        throw new Error("Must specify name for mixin.");
    }

    return wrapper;
};

const BaseMixin = (mixin, name) => wrap(mixin, (target) => {
    let application = mixin(target); // class extends target
    application.prototype[MIXIN_REF] = mixin[ORIGINAL_MIXIN];

    return application;
}, name);

const Cached = (mixin) => wrap(mixin, (target) => {
    // Create a symbol used to reference a cached application from a target
    let applicationRef = mixin[CACHED_APPLICATION_REF];
    if (!applicationRef) {
        applicationRef = mixin[CACHED_APPLICATION_REF] = Symbol(mixin.displayName);
    }

    // Look up an cached application of `mixin` to `target`
    if (target.hasOwnProperty(applicationRef)) {
        return target[applicationRef];
    }

    // Apply the mixin
    let application = mixin(target);

    // Cache the mixin application on the superclass
    target[applicationRef] = application;

    return application;
});


const HasInstance = (mixin, recursive) => {
    if (Symbol.hasInstance && !mixin.hasOwnProperty(Symbol.hasInstance)) {
        Object.defineProperty(mixin, Symbol.hasInstance, {
            value: function instanceOfMixin(o) {
                while (o != null) {
                    // if (recursive && typeof(o) === "object") {
                    //     let testChildren = Object.values(o).some(v => instanceOfMixin(v));
                    //     if (testChildren)  return true;
                    // }

                    if (o.hasOwnProperty(MIXIN_REF) && o[MIXIN_REF] === mixin[ORIGINAL_MIXIN]) return true;
                    o = Object.getPrototypeOf(o);
                }

                return false;
            }
        });
    }
    return mixin;
};