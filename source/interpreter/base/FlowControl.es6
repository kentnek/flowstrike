'use strict';

import Predicate from "./Predicate";
import BaseExpression from "./BaseExpression";

export default class FlowControl extends Predicate(BaseExpression) {

    async evaluate(context, input) {
        if (!this.parent) throw new Error(`"${this.constructor.name}" cannot be the root expression.`);
        if (!this.parent.flow) throw new Error(`"${this.constructor.name}" must be a child of a CompoundExpression.`);

        if (await this.test(context, input) === true) this.alter(this.parent);
    }

    alter(parent) {
        throw new Error("Abstract method not implemented.");
    }
}
