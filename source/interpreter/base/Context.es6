'use strict';

const CONTEXT_SCOPE = Symbol("context_scope");

export default class Context {
    constructor(source) {
        this[CONTEXT_SCOPE] = new Scope();
        if (source) Object.assign(this, source);
    }

    getScope() {
        return this[CONTEXT_SCOPE];
    }

    setScope(scope) {
        if (scope instanceof Scope) {
            this[CONTEXT_SCOPE] = scope;
        } else {
            throw new TypeError("Invalid Scope assigned to context.")
        }
    }
}

const SCOPE_DATA = Symbol("scope_data");

class Scope {
    constructor(fromScope) {
        if (fromScope) {
            if (fromScope instanceof Scope) {
                this[SCOPE_DATA] = Object.assign({}, fromScope[SCOPE_DATA]);
            } else {
                throw new TypeError("Invalid Scope passed to Scope's constructor.")
            }
        } else {
            this[SCOPE_DATA] = {};
        }
    }

    get(name) {
        return this[SCOPE_DATA][name];
    }

    set(name, value) {
        this[SCOPE_DATA][name] = value;
    }

    toString() {
        return JSON.stringify(this[SCOPE_DATA]);
    }

    clone() {
        return new Scope(this);
    }
}