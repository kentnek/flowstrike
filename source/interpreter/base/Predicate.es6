'use strict';

import Mixin from "../utils/Mixin";

const TRUE_PREDICATE = () => true;

export default Mixin((target) => class extends target {
    constructor(predicateFn) {
        super();
        this.predicateFn = predicateFn || TRUE_PREDICATE;
    }

    async test(context, input) {
        let testResult = await this.evaluateStep(this.predicateFn, context, input);
        if (testResult === true) return true;
        if (testResult === false) return false;
        throw new TypeError(`Expecting boolean input, but received ${typeof input}.`);
    }

    // negate() {
    //     this.predicateFn = negate(this.predicateFn);
    //     return this;
    // }

}, "predicate");

// export function negate(fn) {
//     if (fn.resolve) {
//         return {
//             resolve(context, input) {
//                 let resolved = fn.resolve(context, input);
//                 let negatedFn = async(...args) => !(await resolved(...args));
//
//                 negatedFn.displayName = "Not " + resolved.name;
//                 negatedFn.extra = resolved.extra;
//
//                 return negatedFn;
//             }
//         };
//     }
//
//     return async(...args) => !(await fn(...args));
// }
