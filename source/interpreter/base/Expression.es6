'use strict';

import BaseExpression from "./BaseExpression";
import Flow from "../flow/Flow";
import LinearFlow from "../flow/LinearFlow";

import Resolvable from "../resolvable/Resolvable";

import Context from "./Context";
import Incomplete from "./Incomplete";

export default class Expression extends BaseExpression {
    constructor(...steps) {
        super();
        this.steps = steps;
        this.flow = this.generateFlow();
    }

    generateFlow() {
        return new LinearFlow(this.steps);
    }

    [Symbol.iterator]() {
        if (!this.steps) return null;

        let fnIterator = this.generateFlow();

        return {
            next: () => ({
                done: !fnIterator.hasNext(),
                value: fnIterator.next()
            })
        };
    }

    async evaluate(context, input) {
        if (context === null || context === undefined) context = new Context();

        let step;
        let data = input;

        let originalScope = context.getScope();

        this.flow.reset();

        while ((step = this.flow.next(input)) !== Flow.NO_OP) {
            if (step instanceof Incomplete) step = step.build();
            if (step instanceof BaseExpression) step.inherit(this);
            if (step instanceof Expression) context.setScope(originalScope.clone());

            let output = await this.evaluateSubExpression(step, context, data);

            // Continuously feed output to next expression
            // If this step doesn't return anything, let the input pass through
            if (output !== undefined) data = output;

            context.setScope(originalScope);
        }

        return await this.postTransform(input, data);
    }

    async evaluateSubExpression(subExpression, context, input) {
        if (subExpression === undefined) return input;

        if (subExpression instanceof Incomplete) {
            throw new TypeError(`Incomplete expression '${subExpression.constructor.name}'.`);
        }

        let toEvaluate = subExpression;

        if (subExpression instanceof Resolvable) {
            toEvaluate = subExpression.resolve(context, input);
            //if (toEvaluate === undefined) toEvaluate = ;
        }

        this.log(subExpression, this.level);

        if (typeof(toEvaluate) === "function") {
            return await toEvaluate(context, input);
        } else if (toEvaluate instanceof BaseExpression) {
            return await toEvaluate.evaluate(context, input);
        } else {
            return toEvaluate;
        }
    }

    async postTransform(expressionInput, evaluateResult) {
        return evaluateResult;
    }
}