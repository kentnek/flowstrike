'use strict';

import Resolvable from "../resolvable/Resolvable";
import Incomplete from "./Incomplete";

export const LOG_ENABLED = Symbol("log_enabled");
export const LOGGER = Symbol("logger");

export default class BaseExpression {

    constructor() {
        this.level = 0;
        this[LOG_ENABLED] = true;
    }

    setLogger(logger) {
        this[LOGGER] = logger;
    }

    isLogEnabled() {
        return this[LOG_ENABLED];
    }

    setLogEnabled(enabled) {
        this[LOG_ENABLED] = enabled;
    }

    log(message, level) {
        if (!this.isLogEnabled()) return;
        if (!this[LOGGER]) return;
        this[LOGGER](message || "", level || this.level);
    }

    inherit(parent) {
        this.parent = parent;

        this.level = parent.level + 1;
        this[LOGGER] = parent[LOGGER];
        this.setLogEnabled(parent.isLogEnabled());
    }


    async evaluate(context, input) {
        return input;
    }

    async evaluateStep(step, context, input) {
        if (!step) return input;

        if (step instanceof Incomplete) {
            step = step.build();
            if (!step) throw new TypeError(`Incomplete expression '${step.constructor.name}'.`);
        }

        if (step instanceof Resolvable) step = step.resolve(context, input);

        if (step instanceof BaseExpression) {
            throw new TypeError("Step expected, but received " + step.constructor.name);
        } else if (typeof(step) === "function") {
            return await step(context, input);
        } else {
            return step;
        }
    }
}
