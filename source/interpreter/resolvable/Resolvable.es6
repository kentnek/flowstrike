'use strict';

import Mixin from "../utils/Mixin";

export default Mixin((target) => class extends target {
    resolve(context, input) {
        return input;
    }
}, "resolvable");