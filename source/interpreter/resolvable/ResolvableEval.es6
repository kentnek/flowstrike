"use strict";

import Resolvable from "./Resolvable";

export default class ResolvableEval extends Resolvable(Object) {
    constructor(fnString) {
        super();
        this.fnString = fnString;
    }

    resolve(context, input) {
        let resolvedFn = this.fnString.replace(/__[\[\]\w.$]*/g, function (match) {
            let resolved = eval(match).resolve(context, input);
            if (typeof resolved === "string") return `"` + resolved + `"`;
            if (resolved instanceof Resolvable) return resolved.resolve(context, input);
            return resolved;
        });

        return eval(resolvedFn);
    }
}