"use strict";

import Resolvable from "./Resolvable";
import Today from "../expressions/Today";

export default class ResolvableObject extends Resolvable(Object) {

    constructor(object) {
        super();
        this.object = object;
    }

    resolve(context, input) {
        this.resolvedObject = ResolvableObject.resolve(this.object, context, input);
        return this.resolvedObject;
    }

    /**
     * Wraps a value inside a ResolvableObject if needed.
     */
    static wrap(value) {
        if (typeof(value) !== "object" || value instanceof Resolvable || value instanceof RegExp) return value;
        return new ResolvableObject(value);
    }

    static resolve(o, context, input) {
        return dfs(o, context, input);
    }
}

function dfs(o, context, input) {
    if (o === null) return o;
    if (o instanceof Resolvable) return o.resolve(context, input);
    if (typeof (o) !== "object" || o instanceof RegExp) return o;
    if (o instanceof Today) return o.valueOf();

    let tmp = Array.isArray(o) ? [] : {}; // If array, needs to append to empty array instead.
    for (let [k, v] of Object.entries(o)) tmp[k] = dfs(v, context, input);
    return tmp;
}