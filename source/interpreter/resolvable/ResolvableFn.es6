"use strict";

import Resolvable from "./Resolvable";
import ResolvableObject from "./ResolvableObject";
import {generateArgumentMap} from "../utils/argument_map";

const MASK_LOG = Symbol("mask_log");

export default class ResolvableFn extends Resolvable(Object) {
    constructor(fn, ...args) {
        super();
        this.fn = fn;

        this.args = args.map(ResolvableObject.wrap);
        this.identifier = fn.name || "ResolvableFn";
    }

    toString() {
        return this.fn.name;
    }

    resolve(context, input) {
        let resolvedArgs = ResolvableObject.resolve(this.args, context, input);

        if (resolvedArgs.some(a => a instanceof Promise)) {
            let e = new TypeError((this.fn.name ? "'" + this.fn.name + "'" : "This deferred function")
                + " does not accept async steps as input.");

            Error.captureStackTrace(e, this.resolve);

            throw e;
        }


        this.argumentMap = generateArgumentMap(this.fn, resolvedArgs, this[MASK_LOG]);

        return this.fn(...resolvedArgs);
    }

    mask() {
        this[MASK_LOG] = true;
        return this;
    }

    static defer(fn) {
        return new Proxy(fn, {
            apply(fn, This, args) {
                return new ResolvableFn(fn, ...args);
            },

            get(target, property) {
                if (property === 'fn') return fn;
                return Reflect.get(target, property);
            }
        });
    }


}