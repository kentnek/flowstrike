"use strict";

import Resolvable from ".//Resolvable";
import ResolvableFn from ".//ResolvableFn";

const NODE_PATH = Symbol("Placeholder.nodePath");
const APPLY_ARGUMENTS = Symbol("Placeholder.args");
const SOURCE = Symbol("placeholder_source");

export default class Placeholder extends Resolvable(Function) {
    constructor(source = "input") {
        super();
        this[NODE_PATH] = [];
        this[SOURCE] = source;
    }

    getNodePath() {
        return this[NODE_PATH];
    }

    resolve(context, input) {
        let data = this[SOURCE] === "input" ? input : context;
        let prevData;

        for (let key of this.getNodePath()) {
            prevData = data;
            data = data[key];
        }

        if (typeof(data) === "function" && this[APPLY_ARGUMENTS]) {
            return data.call(prevData, ...this[APPLY_ARGUMENTS]);
        } else {
            return data;
        }
    }

    static create(source = "input", currentPlaceholder = new Placeholder()) {
        return new Proxy(currentPlaceholder, {
            get(target, key) {
                // Create a new Placeholder for every first property extraction
                if (target.getNodePath().length === 0) target = new Placeholder(source);

                if (key in target && key !== "length") return target[key];  // skips ownProperty like getNodePath() and resolve()
                target[NODE_PATH].push(key);            // appends to path
                return Placeholder.create(source, target);      // Proxify again for multi-level extraction
            },

            apply(target, thisArg, argList) {
                target[APPLY_ARGUMENTS] = argList;
                return target;
            }
        });
    }
}
