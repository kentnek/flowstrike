import Flow from "./Flow";

export default class LinearFlow extends Flow {
    constructor(steps) {
        super();
        this.setSteps(steps);
    }

    setSteps(steps) {
        this.steps = steps;
        this.reset();
    }

    hasNext() {
        return this.steps && (this.position + 1 < this.steps.length);
    }

    next() {
        if (this.hasNext()) {
            this.position++;
            return this.steps[this.position];
        } else {
            return Flow.NO_OP;
        }
    }

    reset() {
        this.position = -1;
    }

    moveToStart() {
        this.position = -1;
    }

    terminate() {
        this.position = this.steps.length;
    }
}