const NO_OP = Symbol("flow.no_op");

export default class Flow {

    hasNext(input) {
        throw new Error("Abstract method not implemented.");
    }

    next(input) {
        throw new Error("Abstract method not implemented.");
    }

    reset() {
        throw new Error("Abstract method not implemented.");
    }

    moveToStart() {
        throw new Error("Abstract method not implemented.");
    }

    terminate() {
        throw new Error("Abstract method not implemented.");
    }

    static get NO_OP() {
        return NO_OP;
    }
}
