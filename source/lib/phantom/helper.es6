'use strict';

import Path from 'path';
import Fs from 'fs';
import {waitFor} from "../utils/async";

//region Render page

function getAssetFile(name, type, ext) {
    let assetFolder = Path.resolve(process.cwd(), `./assets`);
    if (!(Fs.existsSync(assetFolder))) Fs.mkdirSync(assetFolder);

    let folder = Path.resolve(assetFolder, `./${type}`);
    if (!(Fs.existsSync(folder))) Fs.mkdirSync(folder);

    let relativeFilePath = `./assets/${type}/${name}` + (ext ? `.${ext}` : "");
    return {relative: relativeFilePath, absolute: Path.resolve(process.cwd(), relativeFilePath)};
}

export async function savePageHtml(page) {
    let {relative, absolute} = getAssetFile(new Date().getTime(), "text", "html");
    let html = await page.property('content');
    Fs.writeFileSync(absolute, html);
    console.log("info:".green, `saved html -> ${relative}`);
    return absolute;
}

let captureIndex = 0;
export async function capture(page, name = captureIndex++, type = "flow") {
    let {relative, absolute} = getAssetFile(name, type, "png");
    await page.render(absolute);
    console.log("info:".green, `captured ${type} -> ${relative}`);
    return absolute;
}

export async function captureSelector(page, selector, name, type = "selector") {
    let pageRect = await page.property('clipRect');

    let elementRect = await page.evaluate(function (selector) {
        return document.querySelector(selector).getBoundingClientRect();
    }, selector);

    await page.property('clipRect', elementRect);
    let filePath = await capture(page, name, type);
    await page.property('clipRect', pageRect);

    return filePath;
}

//endregion

/**
 * waitForEvaluate(page, phantomFn, timeout)
 * waitForEvaluate(page, [phantomFn, args], timeout);
 */
export async function waitForEvaluate(page, phantomFn, timeout, message) {
    let fn = phantomFn, args = [];

    if (Array.isArray(phantomFn)) {
        fn = phantomFn.shift();
        args = phantomFn;
    }

    await waitFor(page.evaluate.bind(page, fn, ...args), timeout, message);
}


export async function openUrl(page, link) {
    let displayLink = link.length < 80 ? link : link.substring(0, 80) + "…";
    console.log("Opening", displayLink);

    let status = await page.open(link);
    if (status !== 'success') throw new Error("Unable to open page.");
}

//endregion