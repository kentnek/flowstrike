"use strict";

import {waitForPromise, waitUntil} from "../utils/async";
import {
    waitForEvaluate,
    openUrl as openUrlPhantom,
    capture,
    savePageHtml,
    captureSelector as _captureSelector
} from "./helper";

export function screenshot() {
    return async(context) => {
        await capture(context.page);
    }
}

export function captureSelector(selector, name, type) {
    return async(context) => {
        await _captureSelector(context.page, selector, name, type);
    }
}

export function saveHtml() {
    return async(context) => {
        await savePageHtml(context.page);
    }
}

export function switchToMainFrame() {
    return async(context) => {
        await context.page.switchToMainFrame();
    }
}

export function size(width, height) {
    return async(context) => {
        await context.page.property('viewportSize', { width, height });
    }
}

export function initializePage() {
    return async(context) => {
        await context.page.invokeMethod("initialized");
    }
}

export function waitForPageLoadFinished(timeout = "5s") {
    return async(context) => {
        await waitForPromise(
            new Promise(r => context.events.once("loadFinished", r)),
            timeout,
            "waitForPageLoadFinished timed out."
        );
    };
}

export function waitForDocumentReady(timeout = "5s") {
    return async(context) => {
        await waitForEvaluate(
            context.page,
            function () {return document.readyState === "complete"},
            timeout,
            "waitForDocumentReady timed out."
        );
    }
}

export function waitForResources(delay = "1s") {
    return async(context) => {
        await waitUntil(context.events, "resourceReceived", delay);
    };
}

export function waitForSelector(selector, timeout = "5s") {
    return async(context) => {
        await waitForEvaluate(
            context.page,
            [function (selector) {return document.querySelector(selector) !== null}, selector],
            timeout,
            `waitForSelector("${selector}") timed out.`
        );
    };
}

export function waitForElementGone(selector, timeout = "5s") {
    return async(context) => {
        await waitForEvaluate(
            context.page,
            [function (selector) {
                var element = document.querySelector(selector);
                return (!element) || element.style.display === "none";
            }, selector],
            timeout,
            `waitForElementGone("${selector}") timed out.`
        )
    };
}

//endregion

//region Actions

export function click(selector) {
    return async(context) => {
        await context.page.evaluate(function (selector) {
            document.querySelector(selector).click();
        }, selector);
    };
}

export function submit(selector) {
    return async(context) => {
        await context.page.evaluate(function (selector) {
            document.querySelector(selector).submit();
        }, selector);
    };
}

export function clickLink(selector) {
    return async(context) => {
        await context.page.evaluate(function (selector) {
            dispatchMouseEvent(selector, "click");
        }, selector);
    };
}

export function mousedown(selector) {
    return async(context) => {
        await context.page.evaluate(function (selector) {
            dispatchMouseEvent(selector, "mousedown");
        }, selector);
    };
}

export function fill($selector, value) {
    return async(context) => {
        await context.page.evaluate(function (selector, value) {
            document.querySelector(selector).value = value;
        }, $selector, value);
    };
}

export function property($selector, $property, value) {
    return async(context) => {
        return await context.page.evaluate(function (selector, property, value) {
            if (value) {
                document.querySelector(selector)[property] = value;
            } else {
                return document.querySelector(selector)[property];
            }
        }, $selector, $property, value);
    };
}

export function openUrl(url) {
    return async(context) => {
        await openUrlPhantom(context.page, url);
    };
}

export function post($url, body) {
    return async(context) => {
        let status = await context.page.open($url, 'POST', body);
        if (status !== 'success') throw new Error("Page post failed.");
    }
}

export function evaluate(fn) {
    return async(context) => {
        return await context.page.evaluate(fn);
    };
}

export function js(script, hasResult = false) {
    return async(context) => {
        let ret = await context.page.evaluateJavaScript(`function() { ${script} }`);
        if (hasResult) return ret;
    };
}

/**
 * Switch to frame with specified name (NOT id).
 */
export function switchToFrame(name) {
    return async(context) => {
        await context.page.switchToFrame(name);
        await context.page.invokeMethod("initialized");
    };
}

//endregion

//region Page Contents

export function exists(selector) {
    return async(context) => {
        return await context.page.evaluate(function (selector) {
            return !!document.querySelector(selector);
        }, selector);
    };
}

export function parseElement(selector, transform = (x => x)) {
    return async(context) => {
        let result = await context.page.evaluate(function (selector) {
            var element = document.querySelector(selector);
            return element ? element.innerText.trim() : "";
        }, selector);

        return transform(result);
    };
}

export function getAttribute(selector, attribute, defaultValue = "") {
    return async(context) => {
        return await context.page.evaluate(function (selector, attribute, defaultValue) {
            var element = document.querySelector(selector);
            return element ? element.getAttribute(attribute) : defaultValue;
        }, selector, attribute, defaultValue);
    };
}

export function parseTable(table, row, column, opts = {}) {
    if (typeof(table) !== "string") throw new TypeError("tableSelector must be a string.");

    Object.keys(opts).forEach(k => {
        if (typeof opts[k] === "function") {
            opts[k] = "fn:" + String(opts[k]);
        }
    });

    return async(context) => {
        return await context.page.evaluate(function (tableSelector, rowSelector, columnSelector, opts) {
            var table = document.querySelector(tableSelector);
            var data = [];

            if (!table) {
                console.log("parseTable: No table found!");
                return data;
            }

            function parse(column, opt) {
                var columnText = column.innerText.trim();

                if (opt === "link") {
                    return column.querySelector('a').href;
                } else if (opt === "multiline") {
                    return columnText.split('\n');
                } else if (opt === "float") {
                    return parseFloat(columnText);
                } else if (opt === "float") {
                    return parseFloat(columnText);
                } else if (typeof(opt) === "function") {
                    return opt(column);
                } else {
                    return columnText;
                }
            }

            var rows = table.querySelectorAll(rowSelector);

            for (var i = 0; i < rows.length; ++i) {
                var rowData = [];
                var columns = rows[i].querySelectorAll(columnSelector);

                for (var j = 0; j < columns.length; ++j) {
                    var opt = opts[j], column = columns[j];

                    if (Array.isArray(opt)) {
                        rowData.push(opt.map(function (o) {return parse(column, o)}));
                    } else {

                        if (typeof(opt) === "string" && opt.indexOf("fn:") === 0) {
                            fn = opt.slice(opt.indexOf("{") + 1, opt.lastIndexOf("}"));
                            fn = new Function("column", fn);
                            opts[j] = fn;
                            opt = fn;
                        }

                        rowData.push(parse(column, opt));
                    }

                }

                data.push(rowData);
            }

            return data;

        }, table, row || "tr", column || "td", opts);
    }
}

//endregion