'use strict';

export default class TimeoutError extends Error {
    constructor(message) {
        super();
        Error.captureStackTrace(this, this.constructor);

        this.name = 'TimeoutError';
        this.message = message;
    }
}