"use strict";

import Moment from 'moment';
import {sleep as sleepAsync} from "../utils/async";
import crypto from 'crypto';

export function sleep(duration = "5s") {
    return async() => {
        await sleepAsync(duration);
    };
}

//region String

export function join(...s) {
    return s.join("");
}

export function joinDash(...s) {
    return s.join("-");
}

export function remove(text, subText) {
    return text.replace(new RegExp(subText, "g"), "");
}

export function execRegex(text, regex) {
    return regex.exec(text);
}

//endregion

//region Parser

export function parseAmount(text) {
    return parseFloat(text);
}

export function parseAmountWithComma(text) {
    return parseFloat(text.replace(/,/g, '') || 0);
}

export function parseDate(dateString, fromFormat = "DD/MM/YYYY", toFormat = "YYYY-MM-DD") {
    return Moment(dateString, fromFormat).format(toFormat);
}

//endregion

export function coalesce(...values) {
    for (let x of values) {
        if (typeof x === "string") {
            if (x.length > 0) return x; else continue;
        }

        if (typeof x === "number") {
            if (x !== 0) return x;
        }

        if (x) return x;
    }

    return null;
}

export function not(x) {
    return !x;
}

export function iif(predicate, trueValue, falseValue) {
    return predicate ? trueValue : falseValue;
}

export function neq(x, y) {
    return x !== y;
}

export function eq(x, y) {
    return x === y;
}

export function lt(x, y) {
    return x < y;
}
export function lte(x, y) {
    return x <= y;
}

export function gt(x, y) {
    return x > y;
}

export function gte(x, y) {
    return x >= y;
}

export function mul(a, b) {
    return a * b;
}

export function add(a, b) {
    return a + b;
}

export function log(...s) {
    console.log(...s);
}

export function dump() {
    return (context, input) => {
        console.log(input);
    }
}

export function defineContext(name, value) {
    if (typeof(name) !== "string") throw new TypeError("name must be a string.");

    return (context) => {
        context[name] = value;
    }
}

export function getContext(name) {
    if (typeof(name) !== "string") throw new TypeError("name must be a string.");

    return (context) => {
        return context[name];
    }
}

export function deleteKey(name) {
    return (context, input) => {
        if (typeof(input) !== "object") throw new TypeError("input must be an object.");

        Reflect.deleteProperty(input, name);
    }
}

export function set(key, value) {
    return (context, input) => {
        input[key] = value;
    }
}

export function deleteTempKeys() {
    return (context, data) => {
        if (typeof(data) !== "object") throw new TypeError("Input must be an object for deleteTempKeys.");

        Object.keys(data).forEach(key => {
            if (key[0] === "$") Reflect.deleteProperty(data, key);
        });

        return data;
    };
}

export function toArray() {
    return (context, input) => {
        return [].concat(input);
    }
}

export function hash(data, algorithm = "md5", encoding = "hex") {
    if (typeof data !== "string") data = JSON.stringify(data);
    return crypto.createHash(algorithm).update(data).digest(encoding);
}