'use strict';

import {toMs} from "./time";
import TimeoutError from "../core/TimeoutError";

export async function sleep(ms) {
    if (typeof ms === "string") ms = toMs(ms);
    return new Promise(r => setTimeout(r, ms));
}

// Wait for predicate to return true.
export async function waitFor(predicate, timeout = "5s", message) {
    let start = new Date().getTime();

    if (typeof timeout === "string") timeout = toMs(timeout);

    while (true) {
        if (await predicate() === true) return;

        let elapsed = new Date().getTime() - start;
        if (elapsed > timeout) throw new TimeoutError(message);

        await sleep(100);
    }
}

// Wait until an event is not emitted after a period.
export async function waitUntil(emitter, event, inactivePeriod) {
    let eventEmitted = false;
    let listener = () => eventEmitted = true;
    emitter.on(event, listener);

    while (true) {
        await sleep(inactivePeriod);

        if (eventEmitted) {
            eventEmitted = false;
        } else {
            break;
        }
    }

    emitter.removeListener(event, listener);
}

export async function waitForPromise(promise, timeout, message) {
    let promiseResolved = false;
    promise.then(() => {promiseResolved = true});
    await waitFor(() => promiseResolved, timeout, message);
}
