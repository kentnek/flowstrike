
const durationPattern = /([\d.]+)(\D+)/;

export function toSeconds(duration) {
    let [,num, unit] = durationPattern.exec(duration);

    if (num === undefined || unit === undefined) throw new Error("Invalid parameters!");

    let multiplier;

    if (unit === "s") multiplier = 1;
    else if (unit === "m" || unit === "min") multiplier = 60;
    else if (unit === "h") multiplier = 3600;
    else throw new Error("Invalid unit!");

    return num * multiplier;
}

export function toMs(duration) {
    return toSeconds(duration) * 1000;
}