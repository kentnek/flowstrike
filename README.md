**Flowstrike** is a utility library that provides an elegant but powerful way to execute function chains and manipulate data.

Inspired by [caolan's async library](http://caolan.github.io/async/docs.html) and more specifically its `waterfall` method, Flowstrike focuses on *waterfall*-like chains of functions, and the manner data flows from one function to another. 

And while caolan's async is *callback*-oriented, Flowstrike utilizes ES7's `async`/`await` to make codes more succinct and natural, entirely avoiding problems such as callback hells.